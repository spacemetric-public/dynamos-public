# Dynamos public

Public repository for the Spacemetric Dynamos application

## Python client
The python client has two applications: generate.py and realize.py. Both these applications can be run with the -h flag for help on parameters.  

### generate.py
The first application *generate.py* is used to generate and store the mosaic definition. The input to the function is the Dynamos API-key, the input file as well as output file. 

The input file to the generate function is a JSON file with a set of parameters that control which images should be added to the mosaic definition as well as some processing parameters and other information.
| element | data type | required | description |
| ------- | --------- | -------- | ----------- |
| name | String | yes | Descriptive name. Used as file names and for display purposes in interfaces. |
| imagePriority | Enum | no | String enumeration of possible image sort orders. Sort order determines the stack order in a mosaic. First image is placed on top. Possible values are:<ul><li> SEQ: Sequential sorting (the order provided in the mosaic definition).</li><li> MRP: Most recent image on top.</li><li> LCC: Least cloud cover on top. </li><li> CTC: Closest to map frame center on top. </li><li> CTT: Image with center Closest To the processing Tile on top. </li><li> COMPOSITE_MEAN: Takes the mean value out of all non-nodata pixels. Creates a composite instead of mosaic. <//li><li> COMPOSITE_MEDIAN: Takes the median value out of all non-nodata pixels.Creates a composite instead of mosaic.</ul> Defaults to SEQ. |
| linMin | double[] | no | Sets the array (per band in bands array) of DN to be rescaled to 0. Setting this value will rescale the output images to 8-bit. If not set the output images will be 16-bit. The same rescale parameters will be applied to all images. Must be set if linMax is set. |
| linMax | double[] | no | Sets the array (per band in bands array) of DN to be rescaled to 255. Setting this value will rescale the output images to 8-bit. If not set the output images will be 16-bit. The same rescale parameters will be applied to all images. Must be set if linMin is set. |
| bands | int[] | no | The band index to use for the output image (zero indexed). |
| cloudMaskNodata | int[] | no | Values in the cloudmask (L2A Scene Classification Layer) that should be treated as nodata (i.e. masked out) in the output image. The same values will be applied to all images. If not specified, no cloudmasking will be applied. |
| cloudMaskDilationArray | int[] | no | Dilation kernel radius in pixels to apply for the Scene Classification Layer pixel values that should be treated as no-data in the output (typically clouds), on a per-classification basis. Must be of the same length as the cloudMaskNodata input.<br /> Example: When masking the SCL values 0, 3 and 8 (cloudMaskNodata: [0,3,8]) we can apply a dilation array [0,1,5] where all the SCL pixel values of 0 will be masked out from the output,  all the SCL pixel values of 3 will be masked out from the output as well as all of the pixel values to the top, bottom, left and right of those values (dilation radius 1) and finally all the SCL pixel values of 8 will be masked out with a 5 radius dilation kernel applied for this pixel value.<br /> This allows for providing larger dilation kernels for certain classifications that are known to have more problematic borders. |
| tileMatrixSet | TileMatrixSet | yes | TileMatrixSet used for data retrieval using WMTS |
| postProcessing | Enum | no | String enumeration of postprocessing. Currently allowed values: NDVI, ACORVI: Will create a one band NDVI product. If set then other band selection parameters will be disregarded. |
| time | String | yes | Only images matching this time criteria will be added to the mosaic. Timestring as defined by “datetime” in https://stacspec.org/STAC-api.html#operation/postSearchSTAC |
| bbox | double[] | yes | Only images matching this AOI will be added to the mosaic. Array of numbers as defined by “bbox” in https://stacspec.org/STAC-api.html#operation/postSearchSTAC |
| maxCloud | int | no | Only images with an estimated cloud percent lower or equal to this value will be added to the mosaic. |

TileMatrixSet data type:
| element | data type | required | description |
| ------- | --------- | -------- | ----------- |
| identifier | String | yes | Id of the TileMatrixSet such that it can be selected in the client. |
| crs | int | yes | EPSG code of the crs used by the Tile Matrix set | 
| tileMatrices | TileMatrix[] | yes | List of usable TileMatrices |

TileMatrix data type:
| element | data type | required | description |
| ------- | --------- | -------- | ----------- |
| identifier | String | yes | Id of the TileMatrix such that it can be selected in the client. |
| scaleDenominator | double | yes | Scale denominator (see WMTS specification OGC 07-057r7, chapter 6.1) |
| topLeftCornerX | double | yes | X coordinate in the provided coordinate reference system for the top left corner of the top left pixel. X is the first axis defined by the coordinate reference system. |
| topLeftCornerY | double | yes | Y coordinate in the provided coordinate reference system for the top left corner of the top left pixel. Y is the second axis defined by the coordinate reference system. |
| tileWidth | int | no | Width of the tiling scheme in pixels. Defaults to 1024. |
| tileHeight | int |  no | Height of the tiling scheme in pixels. Defaults to 1024. |
| matrixWidth | int | no | Width of the matrix in number of tiles. This is required for interoperability with external WMTS clients but otherwise not required since the mosaic extent is defined by the user. |
| matrixHeight | int | no | Height of the matrix in number of tiles. This is required for interoperability with external WMTS clients but otherwise not required since the mosaic extent is defined by the user. |


The file may look like this
```
{
    "name": "my-dynamos-mosaic",
    "imagePriority": "LCC",
    "time": "2020-05-01T00:00:00Z/2020-07-31T23:59:59Z",
    "bbox": [
        14.14,
        57.06,
        14.29,
        56.96
    ],
    "maxCloud": 60,
    "bands": [
        1,
        2,
        3,
        7,
        10
    ],
    "cloudMaskNodata": [
        0,
        3,
        8,
        9,
        10,
        11
    ],
    "cloudMaskDilationArray": [
        20,
        2,
        20,
        20,
        20,
        10
    ],
    "tileMatrixSet": {
        "identifier": "Sweref99",
        "crs": 3006,
        "tileMatrices": [
            {
                "identifier": "TestArea",
                "scaleDenominator": 35714.2857,
                "topLeftCornerX": 6324400,
                "topLeftCornerY": 447000,
                "tileWidth": 1024,
                "tileHeight": 1024,
            }
        ]
    }
}
```

### realize.py
The *realize.py* application is used to realize the mosaic (create and download the actual tiles).  
When realizing the mosaic one has to provide the api key, mosaic id as well as the tile matrix set and tile matrix identifiers. After this is the tile ranges that should be downloaded. Finally there is the output folder.
It is also possible to specify a few optional parameters. *-p* enables provenance output. This will generate zip files for each tile that contains both the image tile as well as its DIMAP metadata file 
and also a provenance file that together with its DIMAP metadata file can be used to, for each pixel in the output image, trace which input image it was taken from.  
The *-t <int>* parameter allows for specifying how many tiles should be generated in parallel. This defaults to 10.
Example: ```python realize.py <api-key> 720051d3-24d5-4ba6-b09d-da6875160dd6 Sweref99 TestArea 2-10 0-7 c:/dynamos/lps```  
This example will download all of the tiles with X values 2-10 and Y values 0-7 from the tile matrix TestArea.

### 3rd party libraries
The Dynamos Pyton client requires Python3.x (written in Pything 3.10, and using asyncio introduced in 3.4) and requires the following 3rd party libraries
* aiofiles
* aiohttp
* requests