import argparse
import dynamosrequests


parser = argparse.ArgumentParser()
parser.add_argument("api_key", help="API access key for the Dynamos system.")
parser.add_argument("mosaic_id", help="Mosaic ID.")
parser.add_argument("output_file", help="Json file with the complete mosaic definition.")
args = parser.parse_args()

input_id = args.mosaic_id
api_key_param = args.api_key
output_file_param = args.output_file

dynamosrequests.view_mosaic_definition(api_key_param, id, output_file_param)
