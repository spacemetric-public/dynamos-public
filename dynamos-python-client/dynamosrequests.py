import json
import requests
import time


def view_mosaic_definition(api_key, mosaic_id, output_file=None):
    """Download the mosaic definition

    Parameters
    ----------
    api_key : str
        API key for access to the Dynamos service
    mosaic_id : str
        Mosaic ID string
    output_file : str, optional
        Output file name, mosaic definition will be written to this file if provided

    Returns
    -------
    mosaic
        Dictionary containing the mosaic object
    """
    headers = {"x-api-key": api_key, "accept": "application/json"}
    response = requests.get("https://qivkq7upr4.execute-api.us-west-2.amazonaws.com/dynamos/mosaic/" + mosaic_id, headers=headers)
    if response.status_code != requests.codes.ok:
        print("Generation of mosaic definition failed")
        response.raise_for_status()

    output_data = response.json()

    if output_file is not None:
        with open(output_file, "w") as outfile:
            json.dump(output_data, outfile)

    return output_data


def wait_for_import(api_key, mosaic_id):
    """Wait for images used by the mosaic to complete metadata import

    Parameters
    ----------
    api_key : str
        API key for access to the Dynamos service
    mosaic_id : str
        Mosaic ID string
    """
    headers = {"x-api-key": api_key, "accept": "application/json"}
    all_images_imported = False
    while not all_images_imported:
        mosaic_data = view_mosaic_definition(api_key, mosaic_id)
        availability = mosaic_data["availability"]
        images_missing = availability["imagesMissing"]
        if images_missing is not None and len(images_missing) > 0:
            print(f"Found images missing from AWS, Mosaic can not be realized {images_missing}")
            raise Exception(f"Images missing from AWS, Mosaic can not be realized {images_missing}")

        waiting_images = availability["imagesPendingImport"]
        if waiting_images is None or len(waiting_images) == 0:
            all_images_imported = True
        else:
            print(f"Found images that are not yet imported, waiting until they are completed importing {waiting_images}")
            time.sleep(15)