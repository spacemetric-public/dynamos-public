import sys
import asyncio
import aiofiles


class MosaicRequest:
    def __init__(self, api_key, mosaic_id, tile_matrix_set, tile_matrix, provenance, output_folder):
        """Constructor for MosaicRequest

        Parameters
        ----------
        api_key : str
            API key for access to the Dynamos service
        mosaic_id : str
            UUID of the mosaic to create and download tiles from
        tile_matrix_set : str
            ID of the tile matrix set
        tile_matrix : str
            ID of the tile matrix
        provenance : bool
            True if provenance information should be requested
        output_folder : str
            Output folder for all the image tiles

        Returns
        -------
        mosaic_request
            The mosaic request object
        """

        self.api_key = api_key
        self.mosaic_id = mosaic_id
        self.tile_matrix_set = tile_matrix_set
        self.tile_matrix = tile_matrix
        self.provenance = provenance
        self.output_folder = output_folder

    async def download(self, session, x, y):
        """Download specific tile

         Parameters
         ----------
         session : Session
             HTTP session for making requests
         x : int
             Value of the X tile to download (0-indexed)
         y : int
             Value of the Y tile to download (0-indexed)
         """

        accept = "image/tiff"
        provenance_request_string = "none"
        extension = ".tif"
        if self.provenance:
            # When provenacne is enabled the result is always a zipped file
            accept = "application/zip"
            provenance_request_string = "provenance"
            extension = ".zip"

        headers = {"x-api-key": self.api_key, "accept": accept}

        # We are using the asynchronous URL since it allows for requests that take longer than 29 seconds to process.
        # This is important for more complex mosaics and for the first few requests before the Lambda function is warmed up properly
        url = f"https://qivkq7upr4.execute-api.us-west-2.amazonaws.com/dynamos/mosaic/{self.mosaic_id}" \
              f"/wmts/1.0.0/async/deflate/{provenance_request_string}/{self.tile_matrix_set}/{self.tile_matrix}/{x}/{y}{extension}"

        # We can make subsequent requests to the same URL until the image has completed production.
        while await self.__request(session, url, headers):
            pass

    async def __request(self, session, url, headers):
        """Submit data request

                Parameters
                ----------
                session : Session
                    HTTP session for making requests
                url : str
                    Request URL
                headers : dict
                    Http headers

                Returns
                -------
                continue
                    True if the data was not downloaded and another request should be made
                """
        async with session.get(url, headers=headers, allow_redirects=True) as response:
            accept = headers.get("accept")
            if response.headers.get("content-type") != accept:
                if response.status >= 400:
                    if response.status == 404:
                        print(f"Tile for URL {url} not found. It might be outside of the data provided by the mosaic")
                        return False
                    print(f"invalid status code {response.status} from request to {url}")
                    sys.exit(-1)

                # If the content type is not the accepted content type, then we sleep for a bit and ask for another attempt by returning True
                await asyncio.sleep(3)
                return True

            # Content type matches the requested type, time to download the data
            disposition_header = response.headers.get('content-disposition')
            file_name = disposition_header.split("\"")[1]
            file_name = f"{self.output_folder}/{file_name}"
            f = await aiofiles.open(file_name, mode="wb")
            await f.write(await response.read())
            await f.close()

            print(f"downloaded file to {file_name}")
            return False


class MosaicRequestInstance:
    def __init__(self, mosaic_request, x, y):
        self.mosaic_request = mosaic_request
        self.x = x
        self.y = y
