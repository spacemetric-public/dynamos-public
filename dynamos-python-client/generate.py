import argparse
import json
import requests


def generate_mosaic_definition(api_key, input_data, output_file=None):
    """Generate and store a mosaic definition in Dynamos

    Parameters
    ----------
    api_key : str
        API key for access to the Dynamos service
    input_data : dict
        JSON dictionary of the contents of the mosaic definition input file
    output_file : str, optional
        Output file name, mosaic definition will be written to this file if provided

    Returns
    -------
    mosaic_id
        The id of the mosaic object
    """
    headers = {"x-api-key": api_key, "accept": "application/json"}
    response = requests.post("https://qivkq7upr4.execute-api.us-west-2.amazonaws.com/dynamos/mosaic/generate", headers=headers, json=input_data)
    if response.status_code != requests.codes.ok:
        print("Generation of mosaic definition failed")
        response.raise_for_status()

    output_data = response.json()

    if output_file is not None:
        with open(output_file, "w") as outfile:
            json.dump(output_data, outfile)

    return output_data['id']


parser = argparse.ArgumentParser()
parser.add_argument("api_key", help="API access key for the Dynamos system.")
parser.add_argument("input_file", help="Json file with mosaic definition parameters such as bands, tile matrix and so on.")
parser.add_argument("output_file", help="Json file with the complete mosaic definition.")
args = parser.parse_args()

input_data_param = json.load(open(args.input_file))
api_key_param = args.api_key
output_file_param = args.output_file

mosaic_id = generate_mosaic_definition(api_key_param, input_data_param, output_file_param)
print(f"Completed generation of mosaic {mosaic_id}. Mosaic definition is stored in Dynamos and is also available for viewing in the output file")
