import argparse
from types import NoneType
from mosaicrequest import MosaicRequest
from mosaicrequest import MosaicRequestInstance
import dynamosrequests
import asyncio
import aiohttp


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("api_key", help="API access key for the Dynamos system.")
    parser.add_argument("mosaic_id", help="Id of the mosaic")
    parser.add_argument("tile_matrix_set", help="Name of the tile matrix set to realize data from.")
    parser.add_argument("tile_matrix", help="Name of the tile matrix to realize data from.")
    parser.add_argument("tile_range_x", help="Range of tiles in the X direction to realize. 0-indexed with '-' character to separate the values")
    parser.add_argument("tile_range_y", help="Range of tiles in the Y direction to realize. 0-indexed with '-' character to separate the values")
    parser.add_argument("output_folder", help="Folder to place the output images in")
    parser.add_argument("-p", "--provenance", action="store_true", help="Enable provenance output.")
    parser.add_argument("-t", "--tasks",  help="Number of parallel tasks created using asyncio. Defaults to 10")
    args = parser.parse_args()

    api_key = args.api_key
    mosaic_id = args.mosaic_id
    tile_matrix_set = args.tile_matrix_set
    tile_matrix = args.tile_matrix
    tile_range_x = args.tile_range_x.split("-")
    tile_range_y = args.tile_range_y.split("-")
    output_folder = args.output_folder
    provenance = args.provenance
    num_tasks = args.tasks
    if type(num_tasks) == NoneType:
        num_tasks = 10
    else:
        num_tasks = int(num_tasks)

    # Stops printing out lots of errors on windows platforms
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

    asyncio.run(create_and_download_data(api_key, mosaic_id, tile_matrix_set, tile_matrix, output_folder, num_tasks, tile_range_x, tile_range_y, provenance))


async def create_and_download_data(api_key, mosaic_id, tile_matrix_set, tile_matrix, output_folder, num_tasks, tile_range_x, tile_range_y, provenance):
    """Create and download the data for the specified range

    Parameters
    ----------
    api_key : str
        API key for access to the Dynamos service
    mosaic_id : str
        UUID of the mosaic to create and download tiles from
    tile_matrix_set : str
        ID of the tile matrix set
    tile_matrix : str
        ID of the tile matrix
    num_tasks : int
        Number of parallel requests to send to the Dynamos service
    tile_range_x : int[]
        Range of tile matrix X values to create and download tiles from
    tile_range_y : int[]
        Range of tile matrix X values to create and download tiles from
    output_folder : str
        Output folder for all the image tiles
    provenance : bool
        True if provenance information should be requested
    """

    # Validate that all of the images have had their metadata imported correctly for this mosaic
    dynamosrequests.wait_for_import(api_key, mosaic_id)

    mosaic_request = MosaicRequest(api_key, mosaic_id, tile_matrix_set, tile_matrix, provenance, output_folder)
    queue = asyncio.Queue()

    # Put all requests into a queue so that some can be processed in parallel
    for x in range(int(tile_range_x[0]), int(tile_range_x[1]) + 1):
        for y in range(int(tile_range_y[0]), int(tile_range_y[1]) + 1):
            mosaic_request_instance = MosaicRequestInstance(mosaic_request, x, y)
            queue.put_nowait(mosaic_request_instance)

    # Create all the tasks that will process the queue
    sem = asyncio.Semaphore(num_tasks)
    session = aiohttp.ClientSession()
    for i in range(queue.qsize()):
        asyncio.create_task(worker(session, queue, sem))

    # Wait for the queue to empty
    await queue.join()
    await session.close()


async def worker(session, queue, sem):
    # Worker that will process the mosaic request queue
    async with sem:
        mosaic_request_instance = await queue.get()
        await mosaic_request_instance.mosaic_request.download(session, mosaic_request_instance.x, mosaic_request_instance.y)
        queue.task_done()


main()
